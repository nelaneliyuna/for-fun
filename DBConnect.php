<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "nibble_front_end_test";

// Create connection
$dbConn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($dbConn->connect_error) {
  die("Connection failed: " . $dbConn->connect_error);
} 
?>