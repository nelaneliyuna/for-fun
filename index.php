<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nibble Test</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="style.css">
</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">N</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
      <ul class="navbar-nav">
        <?php
          include 'DBconnect.php';

          $queryMenu = "SELECT * FROM menu where is_publish=1";
          $menus = $dbConn->query($queryMenu);

          while($menu = $menus->fetch_assoc()) {
            if($menu['type'] == "button"){
        ?>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo $menu['link']; ?>"><?php echo $menu['name']; ?></a>
              </li>
            <?php
              } 
              elseif ($menu['type'] == "dropdown"){
            ?>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="<?php echo $menu['link']; ?>" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <?php echo $menu['name']; ?>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <?php
                    $querySubMenu = "SELECT * from submenu where menu_id = ".$menu['menu_id']." and is_publish = 1";
                    $subMenus = $dbConn->query($querySubMenu);

                    while($subMenu = $subMenus->fetch_assoc()){
                      echo "<a class='dropdown-item' href='".$subMenu['link']."'>".$subMenu['name']."</a>";
                    }
                ?>
                </div>
              </li>
        <?php
              }
            }
          $dbConn->close();
        ?>
      </ul>
    </div>
    <input type="button" class="button-nav" value="JOIN US"></input>
  </nav>
  <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img class="d-block w-100" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/252820/1320x580-74.jfif" alt="First slide">
        <div class="content">
            <div class="sub-heading">
              BUILD UP YOUR
            </div>
            <div class="heading">
              STRENGTH 
            </div>
            <h3 style="color: white;">Build Your Body and Fitness with Professional Touch</h3>
            <input type="button" class="button" value="JOIN US"></input>
        </div>
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="https://64.media.tumblr.com/b15ef194a8deca5db54b1303633c0113/48db04ad703dd0f7-f1/s500x750/46fc270d721a1b640772d737471b9e48ec9e93c4.png" alt="Second slide">
        <div class="content">
            <div class="sub-heading">
              BUILD UP YOUR
            </div>
            <div class="heading">
              STRENGTH 
            </div>
            <h3 style="color: white;">Build Your Body and Fitness with Professional Touch</h3>
            <input type="button" class="button" value="JOIN US"></input>
        </div>
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/252820/1320x580-77.jfif" alt="Third slide">
        <div class="content">
            <div class="sub-heading">
              BUILD UP YOUR
            </div>
            <div class="heading">
              STRENGTH 
            </div>
            <h3 style="color: white;">Build Your Body and Fitness with Professional Touch</h3>
           <input type="button" class="button" value="JOIN US"></input>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</body>
</html>

<script>
  const $dropdown = $(".dropdown");
  const $dropdownToggle = $(".dropdown-toggle");
  const $dropdownMenu = $(".dropdown-menu");
  const showClass = "show";

  $(window).on("load resize", function() {
    if (this.matchMedia("(min-width: 768px)").matches) {
      $dropdown.hover(
        function() {
          const $this = $(this);
          $this.addClass(showClass);
          $this.find($dropdownToggle).attr("aria-expanded", "true");
          $this.find($dropdownMenu).addClass(showClass);
        },
        function() {
          const $this = $(this);
          $this.removeClass(showClass);
          $this.find($dropdownToggle).attr("aria-expanded", "false");
          $this.find($dropdownMenu).removeClass(showClass);
        }
      );
    } else {
      $dropdown.off("mouseenter mouseleave");
    }
  });
</script>